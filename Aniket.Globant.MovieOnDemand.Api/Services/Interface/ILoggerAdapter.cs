﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aniket.Globant.MovieOnDemand.Api.Services.Interface
{
    public interface ILoggerAdapter<T>
    {
        void LogInformation(string message);
    }
}

