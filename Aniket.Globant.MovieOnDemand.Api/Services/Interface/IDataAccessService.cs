﻿using Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie;
using System.Collections.Generic;

namespace Aniket.Globant.MovieOnDemand.Api.Services.Interface
{
    public interface IDataAccessService
    {
        List<string> GetAllGenerDetails();
        string AddGenres(string[] genres);

        string DeleteGenres (int id);

        List<Movie> GetAllMovieList();

        string AddMovie(Movie movie);

        string UpdateMovieDescrption(int id);
    }
}
