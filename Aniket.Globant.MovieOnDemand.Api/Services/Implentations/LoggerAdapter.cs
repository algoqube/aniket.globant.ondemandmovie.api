﻿using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging; 

namespace Aniket.Globant.MovieOnDemand.Api.Services.Implentations
{
    public class LoggerAdapter<T>:ILoggerAdapter<T>
    {
        private readonly ILogger<T> logger;

        public LoggerAdapter(ILogger<T> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public void LogInformation(string message)
        {
            logger.LogInformation(message);
        }
    }
}
