﻿using Aniket.Globant.MoveOnDemand.Api.Common.Data;
using Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie;
using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using System.Collections.Generic;

namespace Aniket.Globant.MovieOnDemand.Api.Services.Implentations
{
    public class DataAccessService : IDataAccessService
    {
        public string AddGenres(string[] genreNames)
        {
            if (genreNames.Length > 0)
                return DataAccess.AddGenre(genreNames);
            else
                return "Fail";
        }
        public string DeleteGenres(int id)
        {
            return DataAccess.DeleteGenre(id);
        }

        public List<string> GetAllGenerDetails()
        {
            return DataAccess.GetGeners();
        }

        public string AddMovie(Movie movie)
        {
            return DataAccess.AddMovie(movie);
        }

        public List<Movie> GetAllMovieList()
        {
            return DataAccess.GetMovieList();
        }

        public string UpdateMovieDescrption(int id)
        {
            throw new System.NotImplementedException();
        }

    }
}
