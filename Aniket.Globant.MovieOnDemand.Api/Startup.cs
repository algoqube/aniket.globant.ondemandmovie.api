﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Aniket.Globant.MovieOnDemand.Api.Services.Implentations;
using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using Aniket.Globant.WebApi.Security.Interface;
using Aniket.Globant.WebApi.Security.Provider;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Aniket.Globant.MovieOnDemand.Api
{
    public class Startup
    {
        private readonly ILogger<Startup> logger;
        private readonly ILoggerFactory loggerFactory;
        public Startup(IConfiguration configuration,ILoggerFactory loggerFactory)
        {
            this.loggerFactory = loggerFactory?? throw new ArgumentNullException(nameof(loggerFactory));
            Configuration = configuration;
            logger = this.loggerFactory?.CreateLogger<Startup>();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            loggerFactory?.AddLog4Net();

            logger?.LogInformation($"START{MethodBase.GetCurrentMethod().Name}.");

            services.AddOptions();

            services.AddMvcCore()
                .AddApiExplorer()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Version = "v1",
                    Title = "On Demand Movie Api",
                    Description = " Get movie details and user details"
                });

                //config.IncludeXmlComments($"{Assembly.GetExecutingAssembly().Location}");
            });

            ResgisterServices(services);

            logger?.LogInformation($"END{MethodBase.GetCurrentMethod().Name}.");

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();
            const string url = "/swagger/v1/swagger.json";
            const string appName = "Globant Movie on Demand";

            app.UseSwaggerUI(config=> {
                config.SwaggerEndpoint(url, appName);
                });
        }
         private static void ResgisterServices(IServiceCollection services)
        {
            services.AddTransient(typeof(ILoggerAdapter<>), typeof(LoggerAdapter<>));
            services.AddTransient<IAuthenticationProvider, AuthenticationProvider>();
            services.AddTransient<IDataAccessService, DataAccessService>();
        }
    }
}
