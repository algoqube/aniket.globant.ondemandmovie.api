﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aniket.Globant.MovieOnDemand.Api.Utils
{
    public static class ApiExtension
    {
        public static string GetApiRequestDetails(this HttpRequest request) => $"{request?.Method}{request?.Path}";
    }
}
