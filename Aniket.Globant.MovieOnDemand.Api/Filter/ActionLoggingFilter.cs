﻿using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using Aniket.Globant.MovieOnDemand.Api.Utils;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aniket.Globant.MovieOnDemand.Api.Filter
{
    public class ActionLoggingFilter:IAsyncActionFilter
    {
        private readonly ILoggerAdapter<ActionLoggingFilter> logger;

        public ActionLoggingFilter(ILoggerAdapter<ActionLoggingFilter> logger)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            OnActionExecuting(context);
            var resultContext = await next();
            OnActionExecuted(resultContext);
        }
        private void OnActionExecuting(ActionExecutingContext context)
        {
            logger.LogInformation($"Executing API-{context.HttpContext.Request.GetApiRequestDetails()}");
            logger.LogInformation("with paramter-");
            context.ActionArguments.ToList().ForEach(arg => logger.LogInformation($"{arg.Key} = {arg.Value}"));
        }

        private void OnActionExecuted(ActionExecutedContext context)
        {
            logger.LogInformation($"Executed API-{context.HttpContext.Request.GetApiRequestDetails()}");

        }
    }
}
