﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aniket.Globant.MovieOnDemand.Api.Filter
{
    public class AuthorizeAttribute : TypeFilterAttribute
    {
        public AuthorizeAttribute(string module, string resource, string operation)
            : base(typeof(AuthorizeFilter))
        {
            Arguments = new object[]
            {
                module ?? throw new ArgumentNullException(nameof(module)),
                resource ?? throw new ArgumentNullException(nameof(resource)),
                operation ?? throw new ArgumentNullException(nameof(operation)),
            };
        }
    }
}
