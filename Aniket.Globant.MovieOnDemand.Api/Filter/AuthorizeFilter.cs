﻿using Aniket.Globant.MoveOnDemand.Api.Common.Models;
using Aniket.Globant.MoveOnDemand.Api.Common.UserServices.Intefaces;
using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using Aniket.Globant.MovieOnDemand.Api.Utils;
using Aniket.Globant.WebApi.Security.Interface;
using Aniket.Globant.WebApi.Security.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Aniket.Globant.MovieOnDemand.Api.Filter
{
    public class AuthorizeFilter:IAsyncAuthorizationFilter
    {
        public const string UnauthorizeErrorMessage = "you are not authorized to access this URL.";
        public const int UnauthorizedStatusCode = (int)HttpStatusCode.Unauthorized;

        private readonly string module;
        private readonly string resource;
        private readonly string operation;
        private readonly ILoggerAdapter<AuthorizeFilter> logger;
        private readonly IAuthenticationProvider authenticationProvider;
        
        public AuthorizeFilter(string module, string resource, string operation,
            ILoggerAdapter<AuthorizeFilter> logger, IAuthenticationProvider authenticationProvider)
        {
            this.module = module ?? throw new ArgumentNullException(nameof(module));
            this.resource = resource ?? throw new ArgumentNullException(nameof(resource));
            this.operation= operation ?? throw new ArgumentNullException(nameof(operation));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.authenticationProvider = authenticationProvider ?? throw new ArgumentNullException(nameof(authenticationProvider));
          }

        public Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            var loggerMessage = $"Authrozaion-{context.HttpContext.Request.GetApiRequestDetails()} for user";
            logger.LogInformation($"START{ loggerMessage}");

            var apisecurityConfig = ApiSecurityConfig.CreateInstance
                (context.HttpContext.User.Identity,module, resource, operation);

            var isAuthorized = authenticationProvider.IsAuthorized(apisecurityConfig);

            if (!isAuthorized)
            {
                context.Result = new JsonResult(new ApiErrorDetails
                {
                    Message = UnauthorizeErrorMessage,
                    StatusCode = UnauthorizedStatusCode

                });
            }

            logger.LogInformation($"END{loggerMessage}");

            return Task.FromResult(true);
        }
    }
}
