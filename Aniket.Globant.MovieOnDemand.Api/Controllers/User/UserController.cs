﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aniket.Globant.MovieOnDemand.Api.Filter;
using Microsoft.AspNetCore.Mvc;

namespace Aniket.Globant.MovieOnDemand.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        [Authorize("Test", "Test", "Write")]
        public ActionResult<IEnumerable<string>> GetMovieByCategory()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        [Authorize("Test", "Test", "Write")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        [Authorize("Test", "Test", "Write")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        [Authorize("Test", "Test", "Write")]
        public void Delete(int id)
        {
        }
    }
}
