﻿using Aniket.Globant.MoveOnDemand.Api.Common.Models;
using Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie;
using Aniket.Globant.MovieOnDemand.Api.Filter;
using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Aniket.Globant.MovieOnDemand.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovieController : ControllerBase
    {
        private readonly ILoggerAdapter<MovieController> logger;
        private readonly IDataAccessService dataAccessService;

        public MovieController(ILoggerAdapter<MovieController> logger, IDataAccessService dataAccessService)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.dataAccessService = dataAccessService ?? throw new ArgumentNullException(nameof(dataAccessService));
        }


        [HttpGet]
        [Route("get-generlist")]
        [Authorize("Test", "Test", "Write")]
        public IActionResult GetAllMovieGeners()
        {
            logger.LogInformation("Calling dataAccessService.GetAllCategoryDetails");

            var categoryList = dataAccessService.GetAllGenerDetails();
            if (categoryList == null || categoryList.Count == 0)
            {
                var message = $"No genere is present";
                logger.LogInformation(message);
                return NotFound(new ApiErrorDetails { StatusCode = StatusCodes.Status404NotFound, Message = message });
            }
            logger.LogInformation($"returned all gerners");
            return Ok(categoryList);
        }

        // Post api/values
        [HttpPost]
        [Route("add-genres")]
        //[Authorize("Test", "Test", "Write")]
        public IActionResult AddMovieGeners([FromBody] string[] valueArray)
        {
            //var valueArray = new[] { values };
            var message = dataAccessService.AddGenres(valueArray);

            return Ok(message);
        }

        // DELETE api/values/5
        [HttpDelete]
        [Route("delete-genre")]
        //[Authorize("Test", "Test", "Write")]
        public IActionResult DeleteGeners(int id)
        {
            var message = dataAccessService.DeleteGenres(id);

            return Ok(message);

        }

        [HttpPost]
        [Route("add-movie")]
        //[Authorize("Test", "Test", "Write")]
        public IActionResult AddNewMovie([FromBody] Movie movie)
        {
            var message = dataAccessService.AddMovie(movie);
            return Ok(message);
        }

        [HttpGet]
        [Route("get-movie")]
        //[Authorize("Test", "Test", "Write")]
        public IActionResult GetMovieLists()
        {
            //var valueArray = new[] { values };
            var message = dataAccessService.GetAllMovieList();

            return Ok(message);
        }

    }
}
