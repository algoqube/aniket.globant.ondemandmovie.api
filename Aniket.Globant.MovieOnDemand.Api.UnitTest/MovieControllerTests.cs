using Aniket.Globant.MovieOnDemand.Api.Controllers;
using Aniket.Globant.MovieOnDemand.Api.Services.Interface;
using NSubstitute;
using NUnit.Framework;
using System;

namespace Tests
{
    public class MovieControllerTests
    {
        private ILoggerAdapter<MovieController> logger;
        private IDataAccessService dataAccessService;
        private MovieController movieController;

        [SetUp]
        public void Setup()
        {
            logger = Substitute.For<ILoggerAdapter<MovieController>>();
            dataAccessService = Substitute.For<IDataAccessService>();
        }

        [Test]
        public void ShouldThrowArgumentNullExecptionIfNoLoggerProvider()
        {
            Action constructionSubject = () => movieController = new MovieController(null, null);

        }
    }
}