﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.UserServices.Intefaces
{
    public interface IRequestHandler<in TRequest, out TResponse> where TRequest: class,TResponse
    {
        TResponse Handler(TRequest request);
    }
}
