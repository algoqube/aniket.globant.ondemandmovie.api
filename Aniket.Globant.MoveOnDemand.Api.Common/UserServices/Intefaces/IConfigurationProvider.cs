﻿using Aniket.Globant.MoveOnDemand.Api.Common.Configurations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.UserServices.Intefaces
{
    public interface IConfigurationProvider
    {
        ConfigurationSettings ConfigurationSettings { get; set; }
    }
}
