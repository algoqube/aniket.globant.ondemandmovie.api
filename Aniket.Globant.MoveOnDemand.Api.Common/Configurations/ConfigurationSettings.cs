﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Configurations
{
    public class ConfigurationSettings
    {
        public string EnviornmentType { get; set; }
        public string LogDir { get; set; }
        public string ConnectionString { get; set; }
    }
}
