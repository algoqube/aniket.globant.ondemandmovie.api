﻿using Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Data
{
    public static class DataAccess
    {
        private static object categoryLock = new object();
        private static object movieLock = new object();
        private static List<Genre> categoryCache;
        private static List<Movie> movieCache;

        public static List<Genre> GenreList
        {
            get
            {
                lock (categoryLock)
                {
                    if (categoryCache == null)
                    {
                        categoryCache = new GenreDatabase();
                    }
                    return categoryCache;
                }
            }
        }

        public static List<Movie> MovieList
        {
            get
            {
                lock (movieLock)
                {
                    if (movieCache == null)
                    {
                        movieCache = new MovieData();
                    }
                    return movieCache;
                }
            }
        }

        public static List<string> GetGeners()
        {
            return GenreList.Select(x => x.Name).ToList();
        }

        public static string AddGenre(string[] genresNames)
        {
            try
            {
                int index = 0;
                foreach (var name in genresNames)
                {
                    if (!string.IsNullOrEmpty(name))
                    {
                        index = categoryCache.Last().Id + 1;

                        categoryCache.Add(new Genre
                        {
                            Id = index,
                            Name = name
                        });
                    }
                }

                return "success";
            }
            catch (System.Exception)
            {
                return "fail";
            }
        }

        public static string DeleteGenre(int id)
        {
            try
            {
                categoryCache.Remove(new Genre { Id = id });
                return "success";
            }
            catch (System.Exception)
            {
                return "fail";
            }

        }

        public static List<Movie> GetMovieList()
        {
            return MovieList;
        }

        public static string AddMovie(Movie request)
        {
            try
            {
               movieCache.Add(new Movie(
                                 Guid.NewGuid().ToString(),
                                 request.Title,
                                 request.Rating,
                                 request.Director,
                                 request.ReleaseDate.Value));

                return "success";
            }
            catch (System.Exception)
            {
                return "fail";
            }
        }
    }
}
