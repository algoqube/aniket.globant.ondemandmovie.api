﻿using Newtonsoft.Json;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Models
{
    public class ApiErrorDetails
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
