﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie
{
    public class Movie
    {
        public Movie()
        {
        }

        public Movie(string imdbId, string title, decimal rating, string director, DateTime releaseDate)
        {
            ImdbId = imdbId;
            Title = title;
            Rating = rating;
            Director = director;
            ReleaseDate = releaseDate;
        }

        public string ImdbId { get; set; }
        public string Title { get; set; }
        public decimal Rating { get; set; }
        public string Director { get; set; }
        public DateTime? ReleaseDate { get; set; }

    }



    public class MovieData : List<Movie>
    {
        public MovieData()
        {
            Add(new Movie() { ImdbId = "1", Title = "A", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
            Add(new Movie() { ImdbId = "2", Title = "B", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
            Add(new Movie() { ImdbId = "3", Title = "C", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
            Add(new Movie() { ImdbId = "4", Title = "D", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
            Add(new Movie() { ImdbId = "5", Title = "E", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
            Add(new Movie() { ImdbId = "6", Title = "F", Rating = 3m, Director = "test", ReleaseDate = DateTime.Now });
        }

    }
}
