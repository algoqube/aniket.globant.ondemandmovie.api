﻿using System.Collections.Generic;
using System.Linq;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Models.Movie
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class GenreDatabase : List<Genre>
    {
        public GenreDatabase()
        {
            Add(new Genre() { Id = 1, Name = "A" });
            Add(new Genre() { Id = 2, Name = "B" });
            Add(new Genre() { Id = 3, Name = "C" });
            Add(new Genre() { Id = 4, Name = "D" });
            Add(new Genre() { Id = 5, Name = "E" });
            Add(new Genre() { Id = 6, Name = "F" });
        }
    }

}
