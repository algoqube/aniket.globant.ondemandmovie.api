﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.MoveOnDemand.Api.Common.Models.User
{
    public class UserRegistration
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string password { get; set; }
        public string EmailId { get; set; }
        public int MobileNumber { get; set; }

    }
}
