﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Linq;
using System.Reflection;
using Aniket.Globant.WebApi.Security.Model;

namespace Aniket.Globant.WebApi.Security.Utils
{
    public static class XmlFileReader
    {
        private static string xmlFileContent;
        private const string SecurityFileName = "SecuritySecurityPermission_Dev.xml";
        private static void ReadSecurityFileContents(string fileName)
        {
            Stream xmlFileStream = null;
            try
            {
                xmlFileStream = Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream("Aniket.Globant.WebApi.Xml"+fileName);
                using (var streamReader = new StreamReader(xmlFileStream))
                {
                    xmlFileContent = streamReader.ReadToEnd();
                }

            }
            finally
            {

                xmlFileStream?.Dispose();
            }
        }

        public static IEnumerable<string> GetFunctionalGroup(ApiSecurityConfig apiSecurityConfig , bool isReloadConfig=false)
        {
            if(isReloadConfig)
            {
                xmlFileContent = string.Empty;
            }

            if (string.IsNullOrEmpty(xmlFileContent))
                ReadSecurityFileContents(SecurityFileName);

            if (string.IsNullOrEmpty(xmlFileContent))
                return Enumerable.Empty<string>();

            var document = XDocument.Parse(xmlFileContent);
            return document.Elements("Applications").Elements("Application")
                   .Where(x => x.Attribute("Name")?.Value == apiSecurityConfig.Module)
                   .Elements("Resources").Elements("Resourse")
                   .Where(x => x.Attribute("Name")?.Value == apiSecurityConfig.Resource)
                   .Elements("Operations").Elements("Operation")
                   .Where(x => x.Attribute("Name")?.Value == apiSecurityConfig.Operation)
                   .Elements("Groups").Elements("Group")
                   .Select(x => x.Attribute("Name")?.Value).ToList();
        }
    }
}
