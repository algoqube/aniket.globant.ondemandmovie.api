﻿using Aniket.Globant.WebApi.Security.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.WebApi.Security.Interface
{
    public interface IAuthenticationProvider
    {
        bool IsAuthorized(ApiSecurityConfig apiSecurityConfig);
    }
}
