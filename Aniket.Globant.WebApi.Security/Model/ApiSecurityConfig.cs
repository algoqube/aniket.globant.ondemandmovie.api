﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace Aniket.Globant.WebApi.Security.Model
{
    public class ApiSecurityConfig
    {
        public IIdentity Identity { get; set; }
        public string Module { get; set; }
        public string Resource { get; set; }
        public string Operation { get; set; }
        public string Enviornment { get; set; }

        public static ApiSecurityConfig CreateInstance(IIdentity identity,string module, string resourse, string operation)
        {
            return new ApiSecurityConfig
            {
                Identity= identity,
                Module = module,
                Resource = resourse,
                Operation = operation,
            };

        }

    }
}
