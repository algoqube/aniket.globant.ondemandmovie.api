﻿using Aniket.Globant.WebApi.Security.Interface;
using Aniket.Globant.WebApi.Security.Model;
using Aniket.Globant.WebApi.Security.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aniket.Globant.WebApi.Security.Provider
{
    public class AuthenticationProvider : IAuthenticationProvider
    {

        public bool IsAuthorized(ApiSecurityConfig apiSecurityConfig)
        {
            return CheckIfUserExist(apiSecurityConfig);
        }

        private static bool CheckIfUserExist(ApiSecurityConfig apiSecurityConfig)
        {
            var functionalGroups = XmlFileReader.GetFunctionalGroup(apiSecurityConfig);

            return apiSecurityConfig.Identity.IsAuthenticated && UserAccessProvider.GetUserFunctionalGroup(apiSecurityConfig.Identity.Name, functionalGroups);
        }
    }

}
